package model.data_structures;

import model.data_structures.Node;

public class DoubleLinkedList <T extends Comparable<T>>{
	
	private Node primerNodo;
	
	private Node ultimoNodo;
	
	private int sizeLista;
	
	public DoubleLinkedList(){
		
		sizeLista = 0;
	}
	
	public int getSize(){
		return sizeLista;
	}
	
	public boolean isEmpty(){
		
		return sizeLista == 0;
	}
	
	public void agregar( T elementoLista){
		
		boolean agregado = false;
		
		Node<T> nuevoNodo = new Node<T>( elementoLista );
		
		if(primerNodo == null){
			
			primerNodo = nuevoNodo;
			agregado = true;
		}
		
		else if( elementoLista.compareTo((T) primerNodo.darElemento()) < 0){
			
			nuevoNodo.cambiarSiguiente( primerNodo );
			primerNodo = nuevoNodo;
			
			agregado = true;
		}
		
		else if( elementoLista.compareTo((T) primerNodo.darElemento()) > 0){
			
			 Node nodoAnterior = primerNodo;
			 Node nodoActual = primerNodo.darSiguiente();
			 
			 while ( nodoActual != null && elementoLista.compareTo((T) nodoActual.darElemento()) > 0){
				 
				 nodoAnterior = nodoActual;
				 nodoActual = nodoActual.darSiguiente();
			 }
			 
			 if( nodoActual == null || elementoLista.compareTo((T) nodoActual.darElemento()) < 0){
				 
				 nodoAnterior.cambiarSiguiente( nuevoNodo );
				 nuevoNodo.cambiarSiguiente( nodoActual );
				 
				 agregado = true;
			 }
		}
		
		if(agregado)
			sizeLista ++;
				
	}

}
