package model.data_structures;

public class Node <T extends Comparable<T>> {

	private Node anterior;
	
	private Node siguiente;
	
	private T elementoNode;
	
	public Node( T elementoNodo) {
		
		this.elementoNode = elementoNodo;
		
	}
	
	public Node darAnterior() {
		return anterior;
	}
	
	public Node darSiguiente() {
		return siguiente;
	}

	public T darElemento() {
		
		return elementoNode;
	}
	
	

	public void cambiarSiguiente(Node nuevoSig) {
					
		if( siguiente == null) {
			siguiente = nuevoSig;
		}
		
		else {
			
			Node nodoActual = siguiente;
			
			siguiente = nuevoSig;
			nuevoSig.siguiente = nodoActual;
		}
		
	}
	
	
	
	
}
